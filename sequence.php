<?php

	define('Framework', true);
	$titre = 'Séquence';
	$cache = false;
	require_once('includes/init.php');
	$tpl = new Smarty;
	require_once('includes/entete.php');
	
	$wordsManager = new WordsManager($bdd);
	
	$word = $wordsManager->getWord(array('id_w', $_GET['id_w']));
	
	$tpl->assign(array(
		'word' => $word,
		'veritas' => new VeritasWord($word)));
	
	$tpl->display('sequence.html');
	require_once('includes/piedDePage.php');
