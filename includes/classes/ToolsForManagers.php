<?php
#################################################
#						#
#	ToolsForManagers.php			#
#	trait contenant des outils communs	#
#	aux class Managers 			#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;   
	
	trait ToolsForManagers
	{
		private $_bdd;
		
		/**
		 * Méthode setBdd()
		 * Mutateur de l'attribut PDO $_bdd
		 * @param PDO $bdd 
		 * @return void
		 */
		
		public function setBdd (PDO $bdd)
		{
			$this->_bdd = $bdd;
		}
		 
		/**
		 * Méthode escape ()
		 * Permet d'échapper les caractères dangereux d'une chaîne de caractères qu'on intègre à nos requêtes
		 * Attention, cette méthode n'ajoute pas de guillemets autour de la chaîne
		 * @param string $chaine
		 * @return string
		 */
		
		protected function escape ($chaine)
		{
			return substr($this->_bdd->quote($chaine), 1, -1);
		}
		
		/**
		 * Méthode listeColonnes ()
		 * Permet d'obtenir la liste des colonnes d'une ou plusieurs tables
		 * @param array $tables
		 * @return array
		 */
		
		protected function listeColonnes (array $tables)
		{
			$liste = ' WHERE ';
			foreach ($tables as $table) $liste .= ' Table_Name = '.$this->_bdd->quote($table).' OR ';
			$liste = substr($liste, 0, -4);
			
			$champs = array();
			$reqChamps = $this->_bdd->queryWithCache("SELECT column_name FROM information_schema.columns ".$liste, 0, 'listeColonnes'.str_replace(' ', '_', $liste));
			
			while ($SQLchamps = $reqChamps->fetch())
				$champs[] = $SQLchamps['column_name'];
			
			return $champs;
		}
		
		/**
		 * Méthode genereWhere ()
		 * Permet de générer la partie WHERE de la requête SQL
		 * @param array $listeParametres
		 * @param array $champs
		 * @return array
		 */
		 
		protected function genereWhere (array $listeParametres, $champs)
		{
			$where = ' WHERE ';
			$execute = array();
			$count = count($listeParametres);
			
			$operateurs = [DB::DIFFERENT, DB::INFERIEUR, DB::SUPERIEUR, DB::INFERIEUR_OU_EGAL, DB::SUPERIEUR_OU_EGAL, DB::BETWEEN, DB::IN, DB::NOT_IN, DB::LIKE, DB::NOT_LIKE, DB::REGEXP, DB::NOT_REGEXP, DB::EGAL, DB::IS];
			
			$i = 0;
			while ($i < $count)
			{
				if ($listeParametres[$i] == DB::PARENTHESE_O || $listeParametres[$i] == DB::PARENTHESE_F || $listeParametres[$i] == DB::SQL_OR || $listeParametres[$i] == DB::SQL_AND)
				{
					$where .= $listeParametres[$i];
					$i++;
				}
				else if (!empty($listeParametres[$i]) && !empty($listeParametres[$i+1]) && isset($listeParametres[$i+2]) && in_array($listeParametres[$i], $champs) && $listeParametres[$i+1] == DB::IS && in_array($listeParametres[$i+2], [DB::SQL_NULL, DB::NOT_NULL]))
				{
					$where .= $this->escape($listeParametres[$i])." ".DB::IS." ".$listeParametres[$i+2];
						
					$i += 3;
				}
				else if (!empty($listeParametres[$i]) && !empty($listeParametres[$i+1]) && isset($listeParametres[$i+2]) && in_array($listeParametres[$i], $champs) && in_array($listeParametres[$i+1], $operateurs))
				{
					$jeton = "a".md5($listeParametres[$i].$listeParametres[$i+2]);
					$where .= $this->escape($listeParametres[$i])." ".$listeParametres[$i+1]." :".$this->escape($jeton);
					$execute[$this->escape($jeton)] = $listeParametres[$i+2];
						
					$i += 3;
				}
				else if (!empty($listeParametres[$i]) && isset($listeParametres[$i+1]) && in_array($listeParametres[$i], $champs))
				{
					$jeton = "a".md5($listeParametres[$i].$listeParametres[$i+1]);
					$where .= $this->escape($listeParametres[$i])." = :".$this->escape($jeton);
					$execute[$this->escape($jeton)] = $listeParametres[$i+1];
						
					$i += 2;
				}
				else $i++;
			}
			
			return ['where' => $where." ", 'execute' => $execute];
		}
		
		/**
		 * Méthode genereOrderBy ()
		 * Permet de générer la partie ORDER BY de la requête SQL
		 * @param array $ordre
		 * @param array $champs
		 * @return array
		 */
		
		protected function genereOrderBy (array $ordre, array $champs)
		{
			if (is_array($ordre) && key_exists('champ', $ordre) && in_array($ordre['champ'], $champs) && in_array($ordre['sens'], [DB::ORDRE_DESC, DB::ORDRE_ASC]))
				return " ORDER BY ".$ordre['champ']." ".$ordre['sens'];
			else if (is_array($ordre))
			{
			    $order = " ORDER BY ";
			    foreach ($ordre as $value)
			    {
				if (is_array($value) && key_exists('champ', $value) && in_array($value['champ'], $champs) && in_array($value['sens'], [DB::ORDRE_DESC, DB::ORDRE_ASC]))
				    $order .= $value['champ']." ".$value['sens'].', ';
			    }
			    return substr($order, 0, -2);
			}
			return '';
		}
		
		protected function genereGroupBy (array $group, array $champs)
		{
			$retour = " GROUP BY ";
			foreach ($group as $value)
			{
			    if (in_array($value, $champs))
				$retour .= $value.', ';
			    else 
			    {
				$champ = explode('.', $value);
				if (in_array($champ[1], $champs))
				    $retour .= $value.', ';
			    }
			}
			return substr($retour, 0, -2);
		}
		
		/**
		 * Méthode executeRequeteListe ()
		 * Permet de créer et exécuter les requêtes des méthodes getListe ou getObjet des Managers
		 * @param string $requete 
		 * @param array $champs
		 * @param array $listeParametres
		 * @param array $ordre
		 * @param string $limit
		 */
		 
		protected function executeRequeteListe ($requete, $champs = null, $listeParametres = null, $ordre = null, $limit = null, $group = null)
		{
			if ($ordre != null) $orderBy = $this->genereOrderBy($ordre, $champs);
			else $orderBy = '';
			
			if ($group != null) $groupBy = $this->genereGroupBy($group, $champs);
			else $groupBy = '';
			
			if ($listeParametres == null) $req = $this->_bdd->query($requete.$groupBy.$orderBy.$this->escape($limit));
			else if (is_array($listeParametres))
			{
				$finRequete = $this->genereWhere($listeParametres, $champs);
				$req = $this->_bdd->prepare($requete.$finRequete['where'].$groupBy.$orderBy.$this->escape($limit));
				$req->execute($finRequete['execute']);
			}
			return $req;
		}
		
		/**
		 * Méthode genereListe ()
		 * Permet de créer la liste retournée par les méthodes getListe ou getObjet des Managers
		 * @param mixed $req 
		 * @param string $class
		 * @return array
		 */
		
		protected function genereListe ($req, $class)
		{
			$liste = array();
			while ($resultat = $req->fetch(PDO::FETCH_ASSOC))
			{
				$objet = new $class;
				$objet->hydrate($resultat);
				$liste[] = $objet;
			}
			return $liste;
		}
	
	}
