<?php
#################################################
#						#
#	CacheArrayManager.php			#
#	class pour gérer la class		#
#	CacheArray				#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;
	
	class CacheArrayManager extends CacheManager
	{
		private $_debut; // Variable contenant ce qu'il y a à écrire au début du fichier du genre <?php $array = ....
		
		public function __construct ()
		{
			parent::__construct();
			$this->setDebut('<?php if ( !defined(\'Framework\') ) exit; $contenu = ');
		}
		
		public function readCache (Cache $objetCache)
		{
			$this->setExpire(time()-$objetCache->getDuree());
			$this->setChemin($objetCache->getNom().'.php');
			
			if ($this->_memcached !== false) // On vérifie qu'on est connecté au serveur memcached
			{
				if ($this->_memcached->get($this->_chemin) != false) 
				{
					$objetCache->setContenu($this->_memcached->get($this->_chemin));
					return true; 
				}
				else if ($this->_memcached->getResultCode() == Memcached::RES_NOTFOUND) return false;
			}
			
			if(file_exists($this->_chemin) && (filemtime($this->_chemin) > $this->_expire || $objetCache->getDuree() == 0) )
			{
				require($this->_chemin);
				$objetCache->setContenu($contenu);
			}
			else
				return false;
		}
		
		public function writeCache (Cache $objetCache)
		{
			$this->setChemin($objetCache->getNom().'.php');
			
			if ($this->_memcached !== false)
			{
				if ($objetCache->getDuree() != 0) $enregistre = $this->_memcached->set($this->_chemin, $objetCache->getContenu(), $this->_expire);
				else $enregistre = $this->_memcached->set($this->_chemin, $objetCache->getContenu()); 
				
				if ($enregistre) return true;
			}
			
			file_put_contents($this->_chemin, $this->_debut.var_export($objetCache->getContenu(), true).'?>');
		}
		
		public function videCache (Cache $objetCache)
		{
			$this->setChemin($objetCache->getNom().'.php');
			if ($this->_memcached !== false)
			{
				$this->_memcached->delete($this->_chemin);
			}
			if (file_exists($this->_chemin)) unlink($this->_chemin);
		}
		
		public function setDebut ($debut)
		{
			if (!is_string($debut)) 
			{
				trigger_error('Le début du fichier à écrire doit être une chaine de caractère.', E_USER_WARNING);
				return;
			}
			$this->_debut = $debut;
		}
	}
	
?>