<?php
#################################################
#						#
#	FormateTexte.php			#
#	trait pour formater du texte		#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;   
	
	trait FormateTexte
	{
		public function BBCodeToHTML ($texte)
		{
			$texte = htmlspecialchars($texte); // On enlève toutes les codes HTML qui aurait pu être utilisés
			$texte = nl2br($texte);
			
			if (isset($_SESSION['membre'])) $pseudo = $_SESSION['membre']->getPseudo();
			else $pseudo = 'mettez_votre_pseudo_ici';
			$texte = str_replace(':pseudo:', $pseudo, $texte);
			
			// Smilies
			require($GLOBALS['cheminRacine'].'config/smilies.php');
			$texte = str_replace($smiliesIn, $smiliesOut, $texte);
			
			// Balise [code]
			if (!function_exists('escape')) 
			{
				function escape($s) 
				{
					$code = $s[1];
					$code = str_replace($GLOBALS['smiliesOut'], $GLOBALS['smiliesIn'], $code);
					$code = strip_tags($code);
					$code = str_replace("[", "&#91;", $code);
					$code = str_replace("]", "&#93;", $code);
					return '<b>Code:</b><br /><code>'.$code.'</code>';
				}
			}
			$texte = preg_replace_callback('/\[code\](.*?)\[\/code\]/ms', "escape", $texte);
			
			$BBCodeIn = array( 
				'/\[b\](.*?)\[\/b\]/ms',
				'/\[i\](.*?)\[\/i\]/ms',
				'/\[u\](.*?)\[\/u\]/ms',
				'/\[img\](.*?)\[\/img\]/ms',
				'/\[email\](.*?)\[\/email\]/ms',
				'/\[url\="?(.*?)"?\](.*?)\[\/url\]/ms',
				'/\[flash\](.*?)\[\/flash\]/ms',
				'/\[size\="?(.*?)"?\](.*?)\[\/size\]/ms',
				'/\[color\="?(.*?)"?\](.*?)\[\/color\]/ms',
				'/\[quote](.*?)\[\/quote\]/ms',
				'/\[list\=(.*?)\](.*?)\[\/list\]/ms',
				'/\[list\](.*?)\[\/list\]/ms',
				'/\[audioflash\](.*?)\[\/audioflash\]/ms',
				'/\[center\](.*?)\[\/center\]/ms',
				'/\[table\](.*?)\[\/table\]/ms',
				'/\[tr\](.*?)\[\/tr\]/ms',
				'/\[td\](.*?)\[\/td\]/ms',
				'/\[offtop\](.*?)\[\/offtop\]/ms',
				'/\[sup\](.*?)\[\/sup\]/ms',
				'/\[sub\](.*?)\[\/sub\]/ms',
				'/\[\*\](.*?)\[\/\*\]/ms',
				'/\[font\="?(.*?)"?\](.*?)\[\/font\]/ms',
				'/\[right\](.*?)\[\/right\]/ms',
				'/\[\*\]\s?(.*?)\n/ms',
				'/\[video\](.*?)\[\/video\]/ms',
				'/\[img width=(.*?),height=(.*?)\](.*?)\[\/img\]/ms'
			);
			$BBCodeOut = array(
				'<strong>\1</strong>',
				'<em>\1</em>',
				'<u>\1</u>',
				'<img class="bb-image" src="\1" alt="\1" />',
				'<a href="mailto:\1" style="color: #1898D5;">\1</a>',
				'<a href="\1" target="_blank" style="color: #1898D5;">\2</a>',
				'<object width="640" height="400"><param name="movie" value="\1"><embed src="\1" width="640" height="400"></embed></object>',
				'<span style="font-size:\1%">\2</span>',
				'<span style="color:\1">\2</span>',
				'<table width="90%" cellspacing="1" cellpadding="3" border="0" align="center"><tr><td><span class="genmed"><b>Citation:</b></span></td></tr><tr><td class="quote">\1</td></tr></table>',
				'<ol start="\1">\2</ol>',
				'<ul>\1</ul>',
				'<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="200" height="20" id="dewplayer" align="middle"><param name="wmode" value="transparent" /><param name="allowScriptAccess" value="sameDomain" /><param name="movie" value="http://www.parrain-linux.com/dewplayer.swf?mp3=\1&amp;showtime=1" /><param name="quality" value="high" /><param name="bgcolor" value="FFFFFF" /><embed src="http://www.parrain-linux.com/dewplayer.swf?mp3=\1&amp;showtime=1" quality="high" bgcolor="FFFFFF" width="200" height="20" name="dewplayer" wmode="transparent" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed></object>',
				'<span style="text-align: center; display: block;">\1</span>',
				'<table>\1</table>',
				'<tr>\1</tr>',
				'<td>\1</td>', 
				'<span style="font-size:10px;color:#ccc">\1</span>', 
				'<sup>\1</sup>',
				'<sub>\1</sub>',
				'<li>\1</li>',
				'<font face="\1">\2</font>',
				'<span style="text-align: right; display: block;">\1</span>',
				'<li>\1</li>',
				'<iframe src="http://www.youtube.com/embed/\1" width="640" height="480" frameborder="0"></iframe>',
				'<img src="\3" width="\1" height="\2"/>'
			);
			$texte = preg_replace($BBCodeIn, $BBCodeOut, $texte);

			$texte = str_replace("\r", "", $texte); // Suppression des retours de carrière

			if (!function_exists('removeBr')) 
			{
			    function removeBr ($s) { return str_replace("<br />", "", $s[0]); }
			}
					
			$texte = preg_replace_callback('#\[pre\](.*?)\[/pre\]#ms', "removeBr", $texte);
			$texte = preg_replace_callback('/<pre>(.*?)<\/pre>/ms', "removeBr", $texte);
			$texte = preg_replace('/<p><pre>(.*?)<\/pre><\/p>/ms', "<pre>\\1</pre>", $texte);

			$texte = preg_replace_callback('/<ul>(.*?)<\/ul>/ms', "removeBr", $texte);
			$texte = preg_replace('/<p><ul>(.*?)<\/ul><\/p>/ms', "<ul>\\1</ul>", $texte);
			$texte = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $texte);
			$texte = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $texte);
			$texte = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $texte);
			$texte = preg_replace('#\[color=(\#[0-9A-F]{6}|[a-z\-]+)\](.*?)\[/color\]#isU', '<span style="color:$1">$2</span>', $texte);
			$texte = preg_replace('#\[b\](.+)\[/b\]#isU', '<strong>$1</strong>', $texte);
			$texte = preg_replace('#\[pre\](.+)\[/pre\]#isU', '<pre>$1</pre>', $texte);
			$texte = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $texte);
			$texte = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration:line-through;">$1</span>', $texte);
			$texte = preg_replace('#\[u\](.+)\[/u\]#isU', '<span style="text-decoration: underline">$1</span>', $texte);
			$texte = preg_replace('#\[url\](.+)\[/url\]#isU', '<a href="$1" target="_blank" style="color: #1898D5;">$1</a>', $texte);
			$texte = preg_replace('#\[url=(.+)\](.*?)\[/url\]#isU', '<a href="$1" target="_blank" style="color: #1898D5;">$2</a>', $texte);
			$texte = preg_replace('#\[url=(.+)\]#isU', '<a href="$1" target="_blank" style="color: #1898D5;">', $texte);
			$texte = preg_replace('#\[/url\]#isU', '</a>', $texte);
			$texte = preg_replace('#\[quote\](.+)\[/quote\]#isU', '</span><table width="90%" cellspacing="1" cellpadding="3" border="0" align="center"><tr><td><span class="genmed"><b>Citation:</b></span></td></tr><tr><td class="quote">$1</td></tr></table><span class="postbody">', $texte);
			$texte = preg_replace('#\[sup\](.+)\[/sup\]#isU', '<sup>$1</sup>', $texte);
			$texte = preg_replace('#\[sub\](.+)\[/sub\]#isU', '<sub>$1</sub>', $texte);
			$texte = preg_replace('#\[\*\](.+)\[/\*\]#isU', '<li>$1</li>', $texte);
			$texte = preg_replace('#\[font=(.+)\](.+)\[/font\]#isU', '<font face="$1">$2</font>', $texte);
			$texte = preg_replace('#\[center\](.+)\[/center\]#isU', '<span style="text-align: cente">$1</span>', $texte);
			$texte = preg_replace('#\[offtop\](.+)\[/offtop\]#isU', '<span style="font-size:10px;color:#ccc">$1</span>', $texte);
			$texte = preg_replace('#\[table\](.+)\[/table\]#isU', '<table class="wbb-table">$1</table>', $texte);
			$texte = preg_replace('#\[tr\](.+)\[/tr\]#isU', '<tr>$1</tr>', $texte);
			$texte = preg_replace('#\[td\](.+)\[/td\]#isU', '<td>$1</td>', $texte);
			
			while (preg_match('#\[quote=(.+)\](.+)\[/quote\]#isU', $texte) !== FALSE)
			{
				$texte = preg_replace('#\[quote=(.+)\](.+)\[/quote\]#isU', '<b>Citation de $1:</b><br /><blockquote>$2</blockquote>', $texte);
			}
			$texte = preg_replace('#\[size=([1-2]?[0-9])\](.*?)\[/size\]#si', '<span style="font-size: $1px; line-height: normal">$2</span>', $texte);
			$texte = preg_replace('#\[list\](.+)\[/list\]#isU', '<ul>$1</ul>', $texte);
			$texte = preg_replace('#\[img\](.+)\[/img\]#isU', '<img alt="$1" src="$1" />', $texte);
			
			$texte = str_replace('<script', '', $texte);
			$texte = trim($texte);

			return $texte;
		}
		
		public function HTMLToBrut ($texte)
		{
			$texte = strip_tags($texte);
			$texte = html_entity_decode($texte);
			$texte = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $texte);
			$texte = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $texte);
			$texte = preg_replace('(\n|\r|\t)',' ',$texte);
			$texte = preg_replace('/\s\s+/', ' ', $texte); 
			return $texte;
		}
	}