 <?php
#################################################
#						#
#	Hashage.php				#
#	trait pour hasher des mots de passe	#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;   
	
	trait Hashage
	{
		public function hashage ($mdp)
		{
			$mdp = md5($mdp);
			$mdp = crypt($mdp, '$6$rounds=10000$'.substr($mdp,16).'$');
			$mdp = substr($mdp,33);
			return $mdp;
		}
		
		public function verificationHashage ($mdp, $hash)
		{
			$mdp = md5($mdp);
			$hash = '$6$rounds=10000$'.substr($mdp,16).'$'.$hash;
			if (crypt($mdp, $hash) == $hash)
				return true;
			else
				return false;
		} 
	}