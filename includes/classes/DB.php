<?php
#################################################
#						#
#	DB.php					#
#	Extension de PDO 			#
#	Création d'un classe DB basée sur 	#
#	PDOEtendu				#
#	Création de l'objet bdd avec 		#
#	cette classe				#
#	Créateur : Guillaume KIHLI		#
#						#
#################################################

	if ( !defined('Framework') ) exit;
	
	define('MYSQL_DATETIME_FORMAT','Y-m-d H:i:s');
	define('MYSQL_DATE_FORMAT','Y-m-d');
	
	class PDOEtendu extends PDO // On étend la class PDO pour pouvoir connaître les temps d'exécution et le nombre de requêtes exécutées
	{
		protected $count = 0;
		protected $racine;
		protected $memoryQuery = array();
		protected $time = 0;
		
		public function count()             { return $this->count; }
		public function memoryQuery()       { return $this->memoryQuery; }
		public function time()              { return $this->time; }
	      
		public function increment()
		{
			$this->count++;
		}
		public function addQuery($query, $time=0)
		{  
			$this->memoryQuery[$this->count()]['query'] = $query;
			$this->memoryQuery[$this->count()]['time'] = $time;
		}
		public function addTime($time)
		{  
			$this->time += $time;
		}
	      
		function __construct($dsn, $username="", $password="", $driver_options=array())
		{
			parent::__construct($dsn,$username,$password, $driver_options);
			
			$this->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatementEtendu', array($this)));      
		}
		
		public function query($query)
		{
			$tmpTemps = microtime(true); 
			$return = parent::query($query);
			$addTime = round(microtime(true) - $tmpTemps,5);
		
			$this->increment();
			$this->addQuery($query,$addTime);
			$this->addTime($addTime);
			return $return;
		}
		
		public function queryWithCache($query, $duree, $nomCache = null)
		{
			if ($nomCache == null) $nomCache = 'sqlCache'.md5($query); 
			$$nomCache = new CacheArray($nomCache, $duree); 
			$cacheArrayManager = new CacheArrayManager;
			if ($cacheArrayManager->readCache($$nomCache) !== false) 
			{
				return $$nomCache;
			}
			else
			{
				$tmpTemps = microtime(true); 
				$return = parent::query($query);
				$addTime = round(microtime(true) - $tmpTemps, 5);
				
				$$nomCache->setContenu($return->fetchAll()); 
				$cacheArrayManager->writeCache($$nomCache); 
			
				$this->increment();
				$this->addQuery($query,$addTime);
				$this->addTime($addTime);
				return $$nomCache;
			}
		}
		
		public function exec($query)
		{
			$tmpTemps = microtime(true); 
			$return = parent::exec($query);
			$addTime = round(microtime(true) - $tmpTemps,5);
			
			$this->addQuery($query,$addTime);
			$this->addTime($addTime);
			$this->increment();
			
			return $return;
		}
	}
	
	class PDOStatementEtendu extends PDOStatement // Même chose que précédemment avec PDOStatement
	{
		protected $pdo;
		
		public function count() { return $this->pdo->count(); }
		
		protected function __construct($_pdo)
		{
			$this->pdo = $_pdo;
		}
	      
		public function execute($input = null)
		{
			$tmpTemps = microtime(true); 
			$return = parent::execute($input);
			$addTime = round(microtime(true) - $tmpTemps, 5);
		
			$this->pdo->increment();
			$this->pdo->addQuery($this->queryString, $addTime);
			$this->pdo->addTime($addTime);
			
			return $return;
		}
		
		public function executeWithCache($input = null, $duree, $nomCache = null)
		{
			if ($nomCache == null) $nomCache = 'sqlCache'.md5($this->queryString); 
			$$nomCache = new CacheArray($nomCache, $duree); 
			$cacheArrayManager = new CacheArrayManager;
			if ($cacheArrayManager->readCache($$nomCache) !== false) 
			{
				return $$nomCache;
			}
			else
			{
				$tmpTemps = microtime(true); 
				$return = parent::execute($input);
				$addTime = round(microtime(true) - $tmpTemps, 5);
				
				$$nomCache->setContenu($this->fetchAll(PDO::FETCH_ASSOC)); 
				$cacheArrayManager->writeCache($$nomCache); 
			
				$this->pdo->increment();
				$this->pdo->addQuery($this->queryString, $addTime);
				$this->pdo->addTime($addTime);
				
				return $$nomCache;
			}
		}
	}
	
	// Cette classe sert à créer l'objet PDOEtendu et à éviter que l'on puisse créer plusieurs fois l'objet PDOEtendu
	// Le getInstance est très utile lorsque l'on est dans une fonction : on peut faire $bdd = DB::getInstance(); pour retrouver son objet $bdd sans se reconnecter à MySQL en créant un nouvel objet PDOEtendu et sans devoir mettre $bdd dans les paramtères de la fonction
	class DB 
	{
		private static $instance = NULL;
		
		const SQL_OR = " OR ";
		const SQL_AND = " AND ";
		const ORDRE_DESC = 'DESC';
		const ORDRE_ASC = 'ASC';
		const EGAL = " = ";
		const DIFFERENT = " != ";
		const INFERIEUR = " < ";
		const SUPERIEUR = " > ";
		const INFERIEUR_OU_EGAL = " <= ";
		const SUPERIEUR_OU_EGAL = " >= ";
		const BETWEEN = " BETWEEN ";
		const IN = " IN ";
		const NOT_IN = " NOT IN ";
		const LIKE = " LIKE ";
		const NOT_LIKE = " NOT LIKE ";
		const REGEXP = " REGEXP ";
		const NOT_REGEXP = " NOT REGEXP ";
		const PARENTHESE_O = " ( ";
		const PARENTHESE_F = " ) ";
		const IS = " IS ";
		const SQL_NULL = " NULL ";
		const NOT_NULL = " NOT NULL ";

		public static function getInstance()
		{
			if (!self::$instance) 
			{
				require_once($GLOBALS['cheminRacine'].'config/bdd.php');
				self::$instance = new PDOEtendu('pgsql:host=127.0.0.1;port=5432;dbname=genetixdb', 'genetixuser', 'JN7m7a2d');
				self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			
			return self::$instance;
		}

		private function __clone() 
		{
		    /* interdiction de cloner l'instance */
		}
	}

?> 
