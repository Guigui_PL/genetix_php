<?php
#################################################
#						#
#	WorkerManager.php			#
#	class pour gérer des workers		#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 
 
	if ( !defined('Framework') ) exit;
	
	class WorkerManager
	{
		private $_listeAttente = array();
		private $_cacheManager;
		private $_workers = array();
		private $_processus = array();
		
		public function __construct ()
		{
			$this->_cacheManager = new CacheManager;
		}
		
		public function lancerWorkers() // Pas par la fenêtre, sadique va !
		{
			passthru("php -r ".$this->genereScript()." 2>> /dev/null >> /dev/null &");
		}
		
		public function addWorker (Worker $worker)
		{
			$this->_workers[] = $worker;
		}
		
		public function genereScript ()
		{
			$processus = microtime(true) . mt_rand(0,time()) . mt_rand(0,time());
			$this->addProcessus($processus);
			
			$script = 'define("Framework", true);
			$_SERVER["SERVER_ADDR"] = "127.0.0.1";
			$_SERVER["HTTP_USER_AGENT"] = "SHELL";
			$_SERVER["REMOTE_ADDR"] = "127.0.0.1";
			$_SERVER["REQUEST_URI"] = "'.$_SERVER["REQUEST_URI"].'";
			$_SERVER["REQUEST_METHOD"] = "GET";
			$cache = false;
			require_once('.$GLOBALS['cheminRacine'].'"includes/init.php"); 
			$cacheManager = new CacheManager();
			$liste = array();';
			
			foreach ($this->_workers as $worker)
			{
				$objetSerialise = serialize($worker->getObjet());
				$script .= '$objet = unserialize(\''.$objetSerialise.'\'); ';
				
				$count = count($worker->getMethode());
				if ($count > 1)
					foreach ($worker->getMethode() as $clef => $valeur)
					{
						if ($clef == 0) $script .= '$retour = $objet->'.$valeur.'(';
						else if ($clef == ($count-1)) $script .= var_dump($valeur).');';
						else $script .= var_dump($valeur).',';
					}
				else $script .= '$retour = $objet->'.$worker->getMethode()[0].'(); ';
				
				if ($worker->getAttendre()) 
				{
					$this->addListeAttente($worker->getId(), $processus);
					$script .= '$liste["'.$worker->getId().'"] = $retour;';
				}
			}
			
			$script .= '$cache = new Cache("'.$processus.'", 120);
				$cache->setContenu($liste);
				$cacheManager->writeCache($cache);';
				
			unset($this->_workers);
			$this->_workers = array();
			
			return escapeshellarg(preg_replace("/(\r\n|\n|\r|\t)/", "", $script));
		}
		
		public function addProcessus ($processus)
		{
			$this->_processus[$processus] = array();
		}
		
		public function addListeAttente ($id, $processus)
		{
			$this->_listeAttente[$id] = $processus;
		}
		
		public function attendreUnWorker ($id)
		{
			$execute = false;
			$timeOut = time()+5;
			
			while (!$execute && $timeOut > time()) 
			{
				$execute = $this->recupererWorker($id, $this->_listeAttente[$id]);
			}
			
			if ($timeOut < time()) return false;
			else return $execute;
		}
		
		public function recupererWorker ($id, $processus)
		{
			if (!empty($this->_processus[$processus]))
			{
				unset($this->_listeAttente[$id]);
				return $this->_processus[$processus][$id];
			}
			else 
			{
				$cache = new Cache(strval($processus), 120);
				$retour = $this->_cacheManager->readCache($cache);
				if ($retour) 
				{
					$this->_processus[$processus] = $cache->getContenu();
					unset($this->_listeAttente[$id]);
					return $this->_processus[$processus][$id];
				}
				else return false;
			}
		}
		
		public function attendreTousWorkers ()
		{
			$listeRetours = array();
			
			$timeOut = time()+5;
			while (!empty($this->_listeAttente) && $timeOut > time())
			{
				foreach ($this->_listeAttente as $id => $processus)
				{
					$worker = $this->recupererWorker($id, $processus);
					if ($worker) { echo round(microtime(true)-$_SERVER["REQUEST_TIME_FLOAT"], 4).'<br />';
					$listeRetours[$id] = $worker;
					}
				}
			}
			
			return $listeRetours;
		}
	}