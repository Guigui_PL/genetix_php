<?php
#################################################
#						#
#	Configuration.php			#
#	class pour représenter un fichier	#
#	de configuration			#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;  
	
	class Configuration
	{
		use FormateTexte;
		
		private $_type;
		private $_separation;
		private $_variables;
		private $_contenu;
		private $_nomFichier;
		
		const TYPE_TEXTE 		= 1;
		const TYPE_TABLEAU_TEXTE	= 2;
		const TYPE_PHP			= 3;
		const TYPE_TABLEAU_PHP		= 4;
		
		public function __construct ($type)
		{
			$this->setType($type);
		}
		
		/**
		 * Méthode setType ()
		 * Mutateur de l'attribut type qui définit le type de fichier de configuration
		 * @param int $type
		 * @return bool
		 */
		
		private function setType ($type)
		{
			if (in_array($type, [self::TYPE_TEXTE, self::TYPE_TABLEAU_TEXTE, self::TYPE_PHP, self::TYPE_TABLEAU_PHP]))
			{
				$this->_type = $type;
				return true;
			}
			else return false;
		}
		
		/**
		 * Méthode setSeparation ()
		 * Mutateur de l'attribut $_separation, qui définit ce qui sépare les cases dans un tableau en 2 dimensions écrit dans un fichier texte 
		 * Non obligatoire, ne concerne que les configurations TYPE_TABLEAU_TEXTE
		 * @param string $separation
		 * @return bool
		 */
		
		public function setSeparation ($separation)
		{
			if ($this->_type == self::TYPE_TABLEAU_TEXTE)
			{
				$this->_separation = $separation;
				return true;
			}
			else return false;
		}
		
		/**
		 * Méthode setVariables ()
		 * Mutateur de l'attribut $variables, contenant la liste des variables pour TYPE_PHP + la liste des clés pour TYPE_TABLEAU_PHP
		 * @param array $variables
		 * @return bool
		 */
		
		public function setVariables ($variables)
		{
			if (($this->_type == self::TYPE_PHP || $this->_type == self::TYPE_TABLEAU_PHP) && is_array($variables))
			{
				$this->_variables = $variables;
				return true;
			}
			else return false;
		}
		
		/**
		 * Méthode setContenu ()
		 * Mutateur de l'attribut contenu
		 * @param mixed $contenu
		 * @return bool
		 */
		
		public function setContenu ($contenu)
		{
			if ( ($this->_type == self::TYPE_TEXTE && (!empty($contenu) || is_string($contenu)))
				|| ($this->_type == self::TYPE_TABLEAU_TEXTE && is_array($contenu))  )
			{
				$this->_contenu = $contenu;
				return true; 
			}
			else if (($this->_type == self::TYPE_TABLEAU_PHP || $this->_type == self::TYPE_PHP) && is_array($contenu))
			{
				$this->_contenu = $contenu;
			}
			else return false;
		}
		
		/**
		 * Méthode ajouterElement ()
		 * Permet d'ajouter un élément à une configuration de type tableau php 
		 * @param string $variable
		 * @param mixed $element 
		 * @param mixed $clef
		 * @return bool
		 */
		 
		public function ajouterElement($variable, $element, $clef = null)
		{
			if (!in_array($variable, $this->_variables)) return false;
			if ($clef == null)
			{
				$this->_contenu[$variable][] = $element;
				return true;
			}
			else 
			{
				$this->_contenu[$variable][$clef] = $element;
			}
		}
		
		/**
		 * Méthode setNomFichier ()
		 * Mutateur de l'attribut $_nomFichier
		 * @param string $nomFichier
		 * @return bool
		 */
		
		public function setNomFichier ($nomFichier)
		{
			if (!empty($nomFichier) && is_string($nomFichier))
			{
				$this->_nomFichier = $nomFichier;
				return true;
			}
			else return false;
		}
		
		/**
		 * Méthode estValide ()
		 * Permet de vérifier si l'objet est valide avant de l'écrire
		 * @return bool
		 */
		 
		public function estValide()
		{
			if (!empty($this->_nomFichier) && !empty($this->_contenu))
			{
				if (($this->_type == self::TYPE_TABLEAU_PHP || $this->_type == self::TYPE_PHP) && is_array($this->_contenu)) return true;
				else if (($this->_type == self::TYPE_TEXTE && (!empty($contenu) || is_string($contenu))) || ($this->_type == self::TYPE_TABLEAU_TEXTE && is_array($contenu))) return true;
				else return false;
			}
			else return false;
		}
		
		public function getType () { return $this->_type; }
		public function getSeparation () { return $this->_separation; }
		public function getVariables () { return $this->_variables; }
		public function getContenu ($bbcode = null) 
		{ 
			if ($bbcode == null) return $this->_contenu; 
			else return $this->BBCodeToHTML($this->_contenu);
		}
		public function getNomFichier () { return $this->_nomFichier; }
	}
	
?>