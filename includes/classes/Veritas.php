<?php
#################################################
#						#
#	Veritas.php				#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

    if ( !defined('Framework') ) exit;
    
    abstract class Veritas 
    {
	protected $_outputs		= 0;
	protected $_howManyInputs	= 0;
	
	abstract public function to_string ();
	abstract public function toHTML ();
	abstract public function inputNamesList ();
	
	public function getMinimalDisjunctiveForm ()
	{
	    return Veritas::mcCluskey($this->_outputs, $this->_howManyInputs, $this->inputNamesList());
	}
	
	public static function mcCluskey ($outputs, $howManyInputs, $inputNames)
	{
	    $size = pow(2, $howManyInputs);
	    $allTrue = $allFalse = true;
	    
	    $browse = $outputs;
	    for ($i = 0; $i < $size; ++$i)
	    {
		if ($allTrue && $browse % 2 == 0)
		    $allTrue = false;

		if ($allFalse && $browse % 2 == 1)
		    $allFalse = false;

		if (!$allFalse && !$allTrue)
		    break;
		    
		$browse = $browse >> 1;
	    }

	    if ($allTrue)
		return "1";
	    else if ($allFalse)
		return "0";
		
	    $inputsTrue = $tmp = $finalList = $used = [];

	    $output = $outputs;
	    for ($i = $size-1; $i >= 0; --$i)
	    {
		if ($output % 2 == 1)
		{
		    $nbOfTrue = 0;
		    $browse = $i;
		    for ($j = 0; $j < $howManyInputs; ++$j)
		    {
			if ($browse % 2 == 1)
			    ++$nbOfTrue;
			$browse = $browse >> 1;
		    }

		    $inputsTrue[$nbOfTrue][$i] = 0;
		}
		$output = $output >> 1;
	    }
	    
	    $stop = false;

	    while (!$stop)
	    {
		$stop = true;
		for ($i = 0; $i < $howManyInputs; ++$i)
		{
		    if (key_exists($i, $inputsTrue) && key_exists($i+1, $inputsTrue))
		    {
			foreach ($inputsTrue[$i] as $key => &$it)
			{
			    foreach ($inputsTrue[$i+1] as $key2 => &$it2)
			    {
				if ($it == $it2)
				{
				    if (is_string($key))
					$termValue = explode(",", $key);
				    else
					$termValue = [$key];
					
				    if (is_string($key2))
					$termValue2 = explode(",", $key2);
				    else
					$termValue2 = [$key2];
					
				    $xorResult = $termValue[0] ^ $termValue2[0];

				    if (!($xorResult & ($xorResult-1)))
				    {
					$newTermValue = array_merge($termValue, $termValue2);
					sort($newTermValue);
					$newKey = implode(",", $newTermValue);

					$tmp[$i][$newKey] = end($newTermValue)-$newTermValue[0];
					$used[$key] = true;
					$used[$key2] = true;

					$stop = false;
				    }
				}
			    }
			}
		    }
		}

		foreach ($inputsTrue as $it3)
		{
		    foreach ($it3 as $key => $it4)
		    {
			if (!key_exists($key, $used))
			    $finalList[$i][$key] = $it4;
		    }
		}

		$inputsTrue = $tmp;
		$tmp = $used = array();
	    } 
	    $cross = array();

	    foreach ($finalList as $final)
	    {
		foreach ($final as $key => $it)
		{
		    $tmpStr = "";
		    if (is_string($key))
			$termValue = explode(",", $key);
		    else
			$termValue = [$key];
		    
		    $termValueBitset = $termValue[0];
		    $rmBitset = $it;
		    
		    for ($j = 0; $j < $howManyInputs; ++$j)
		    {
			if ($rmBitset % 2 != 1)
			{
			    if ($termValueBitset % 2 == 0)
				$tmpStr = "!" . $inputNames[$howManyInputs-$j-1] . $tmpStr;

			    else if ($termValueBitset % 2 == 1)
				$tmpStr = $inputNames[$howManyInputs-$j-1] . $tmpStr;

			    $tmpStr = "." . $tmpStr;
			}
			$rmBitset = $rmBitset >> 1;
			$termValueBitset = $termValueBitset >> 1;
		    }

		    $tmpStr = substr($tmpStr, 1);

		    $tmpTab = [];

		    for ($j = 0; $j < count($termValue); ++$j)
		    {
			$tmpTab[$termValue[$j]] = true;
		    }

		    $cross[$tmpStr] = $tmpTab;
		}
	    }

	    $del = false;
	    krsort($cross);

	    foreach ($cross as $key => $it)
	    {
		$tmpTab = $it;

		foreach ($cross as $key2 => $it2)
		{
		    for ($i = 0; $i < $size; ++$i)
		    {
			if ($key != $key2 && key_exists($i, $it) && key_exists($i, $it2) && $it[$i] == 1 && $it2[$i] == 1)
			{
			    $tmpTab[$i] = 0;
			}
		    }
		}

		$del = true;

		foreach ($tmpTab as $it3)
		{
		    if ($it3 == 1)
			$del = false;
		}

		if ($del)
		    unset($cross[$key]);
	    }

	    $tmpStr = "";

	    foreach ($cross as $key => $it)
	    {
		$tmpStr .= $key . " + ";
	    }
	    

	    return substr($tmpStr, 0, -3);
	}
	
	public function outputToString()
	{
	    $size = pow(2, $this->_howManyInputs);
	    $output = $this->_outputs;
	    $string = "";
	    
	    for ($i = 0; $i < $size; ++$i)
	    {
		$string .= $output % 2;
		$output = $output >> 1;
	    }
	    
	    return strrev($string);
	}
	
	public function getMinimalOutput ()
	{
	    $minimal = $this->getMinimalDisjunctiveForm();
	    if ($minimal == "1" || $minimal == "0")
		return $minimal;
		
	    $logic = new Logic($minimal);
	    $veritas = new VeritasLogic($logic);
	    
	    return $veritas->outputToString();
	}
	
	public function getOutput () { return $this->_output; }
    }