<?php
#################################################
#						#
#	Langue.php				#
#	class pour la Langue			#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;   
 
	class Langue 
	{
		private $_langue = 'FR';
		private $_traductions;
		private $_configurationsManager;
		private $_configuration;
		
		public function __construct ($langue)
		{
			$this->setLangue($langue);
			
			require_once($GLOBALS['cheminRacine']."config/langue/".$this->_langue."/langue.php");
			
			$this->setTraductions($traductions);
			$this->setConfigurationsManager();
			$this->setConfiguration();
		}
		
		public function setLangue ($langue)
		{
			if (file_exists("../config/langue/".$langue."/langue.php") || file_exists("config/langue/".$langue."/langue.php")) $this->_langue = $langue;
			else trigger_error('Cette langue n\'existe pas : '.$langue, E_USER_WARNING);
		}
		
		public function setTraductions (array $traductions)
		{
			$this->_traductions = $traductions;
		}
		
		public function setConfigurationsManager ()
		{
			$this->_configurationsManager = new ConfigurationsManager;
		}
		
		public function setConfiguration ()
		{
			$this->_configuration = $this->_configurationsManager->getConfiguration("langue/FR/langue.php");
		}
		
		public function writeConfiguration ()
		{
			$this->_configurationsManager->writeConfiguration($this->_configuration);
		}
		
		public function getTraduction ($phrase, $elements = null)
		{
			if (isset($this->_traductions[$phrase])) $phrase = $this->_traductions[$phrase];
			else $this->_configuration->ajouterElement('traductions', $phrase,$phrase);
			
			if (!empty($elements) && is_array($elements)) 
			{
				foreach ($elements as $element)
					if (strpos($phrase, '%e') !== FALSE) 
						$phrase = substr_replace($phrase, $element, strpos($phrase, '%e'), 2);
			}
			else if (!empty($elements) && is_string($elements)) $phrase = substr_replace($phrase, $elements, strpos($phrase, '%e'), 2);
					
			return $phrase;
		}
		
		public function getLangue () { return $this->_langue; }
	
	}