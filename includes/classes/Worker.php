<?php
#################################################
#						#
#	Worker.php				#
#	class pour créer des tâches asynchrones	#
#	à partir d'objets			#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 
 
	if ( !defined('Framework') ) exit;
	
	class Worker
	{
		private $_objet;
		private $_methode;
		private $_id;
		private $_attendre = true;
		
		public function __construct ()
		{
			$cpt = func_num_args();
			$args = func_get_args();
			
			if ($cpt > 0)
			{
				$this->setObjet(array_shift($args));
				$this->setMethode($args);
			}
		}
		
		public function setObjet ($objet)
		{
			if (!empty($objet) && is_object($objet))
			{
				$this->_objet = $objet;
				return $this->setId();
			}
			else trigger_error("Cet objet n'existe pas !", E_USER_WARNING);
		}
		
		public function setId ()
		{
			$this->_id = microtime(true) . mt_rand(0,time()) . mt_rand(0,time());
			return $this->_id;
		}
		
		public function setMethode ($args)
		{
			if (!empty($this->_objet))
			{
				if (method_exists($this->_objet, $args[0])) $this->_methode = $args;
				else trigger_error("Cet objet ne possède pas cette méthode !", E_USER_WARNING);
			}
			else trigger_error("L'objet n'a pas été défini !", E_USER_WARNING);
		}
		
		public function setAttendre ($attendre)
		{
			if (is_bool($attendre)) $this->_attendre = $attrendre;
			else trigger_error("L'attente doit être un booléen !", E_USER_WARNING);
		}
		
		public function getAttendre () { return $this->_attendre; }
		public function getId () { return $this->_id; }
		public function getObjet () { return $this->_objet; }
		public function getMethode () { return $this->_methode; }
	}