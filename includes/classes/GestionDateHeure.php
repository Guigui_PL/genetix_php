<?php
#################################################
#						#
#	GestionDateHeure.php			#
#	Trait contenant des méthodes pour gérer	#
#	la gestion de la date et de l'ehure	#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;   

	trait GestionDateHeure
	{
		private $_defaultTimeZone = 'Europe/Paris';
		private $_traitTimeZone;
		
		public function setTraitTimeZone ($timeZone = null)
		{
			if ($timeZone == null || !in_array($timeZone, DateTimeZone::listIdentifiers()))  $this->_traitTimeZone = new DateTimeZone($this->_defaultTimeZone);
			else  $this->_traitTimeZone = new DateTimeZone($timeZone);
		}
		
		public function getDateTimeNow ($timeZone = null)
		{
			$this->setTraitTimeZone($timeZone);
			return new DateTime("now", $this->_traitTimeZone);
		}
		
		public function creerDateTime ($dateTime, $timeZone = null)
		{
			$this->setTraitTimeZone($timeZone);
			if ($dateTime == "0000-00-00") $dateTime = "0000-01-01";
			else if ($dateTime == "0000-00-00 00:00:00") $dateTime = "0000-01-01 00:00:00";
			return new DateTime($dateTime, $this->_traitTimeZone);
		}
		
		public function getDateTime (DateTime $dateTime) // Renvoie la date et l'heure avec le fuseau du membre si connecté
		{
			if (!isset($_SESSION['membre'])) $this->setTraitTimeZone($this->_defaultTimeZone);
			else $this->setTraitTimeZone($_SESSION['membre']->getTimeZone());
			return $dateTime->setTimezone($this->_traitTimeZone);
		}
		
		public function getSelectFuseauxHoraire ($defaut = null)
		{
			if ($defaut = null || !in_array($defaut, DateTimeZone::listIdentifiers())) $option = $this->_defaultTimeZone;
			else $option = $defaut;
			
			$retour = '';
			foreach (DateTimeZone::listIdentifiers() as $value)
			{
				if ($value = $defaut)
					$retour .= '<option selected="selected" value="'.$value.'">'.$value.'</option>';
				else
					$retour .= '<option value="'.$value.'">'.$value.'</option>';
			}
			return $retour;
		}
	}