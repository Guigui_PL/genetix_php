<?php
#################################################
#						#
#	WordsManager.php			#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

    if ( !defined('Framework') ) exit;  
    
    class WordsManager
    {
	use ToolsForManagers;
	    
	public function __construct ($bdd)
	{
	    $this->setBdd($bdd);
	}
	    
	public function getListe (Pagination $pagination, $listeParametres = null, $ordre = null)
	{
	    if ($listeParametres != null || $ordre != null)
		$champs = $this->listeColonnes(['words', 'logical_functions', 'word_features']);
	    else $champs = null;
	    
	    $champs[] = "length";
		    
	    $requete = "SELECT id_w, word, nb_genes, ndf, char_length(word) as length 
			    FROM words w 
			    JOIN logical_functions fn ON fn.id_fn=w.id_fn 
			    JOIN word_features wf ON wf.id_wf=w.id_wf  ";
	    
	    $nomCache = md5($requete.serialize($champs).serialize($listeParametres).serialize($ordre).serialize($pagination->getLimit()));
	    
	    $$nomCache = new CacheArray($nomCache, 0); 
	    $cacheArrayManager = new CacheArrayManager;
	    $cacheArrayManager->readCache($$nomCache);
	    
	    if ($cacheArrayManager->readCache($$nomCache) !== false)  return $$nomCache;
	    else
	    {
		$req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $pagination->getLimit());
		$liste = $this->genereListe($req, 'Word');
		
		$$nomCache->setContenu($liste); 
		$cacheArrayManager->writeCache($$nomCache);
		
		return $liste;
	    }
	}
	    
	public function getWord ($listeParametres = null, $ordre = null)
	{
	    if ($listeParametres != null || $ordre != null)
		$champs = $this->listeColonnes(['words', 'logical_functions', 'word_features']);
	    else $champs = null;
	    
	    $champs[] = "length";
		    
	    $requete = "SELECT id_w, word, nb_genes, ndf, char_length(word) as length 
			    FROM words w 
			    JOIN logical_functions fn ON fn.id_fn=w.id_fn 
			    JOIN word_features wf ON wf.id_wf=w.id_wf  ";
	    
	    $nomCache = md5($requete.serialize($champs).serialize($listeParametres).serialize($ordre).serialize("LIMIT 1"));
	    
	    $$nomCache = new CacheArray($nomCache, 0); 
	    $cacheArrayManager = new CacheArrayManager;
	    $cacheArrayManager->readCache($$nomCache);
	    
	    if ($cacheArrayManager->readCache($$nomCache) !== false)  return $$nomCache->getContenu();
	    else
	    {
		$req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, "LIMIT 1");
		$liste = $this->genereListe($req, 'Word');
		
		$$nomCache->setContenu($liste[0]); 
		$cacheArrayManager->writeCache($$nomCache);
		
		return $liste[0];
	    }
	}
	
	public function getNombre ($id_fn = null)
	{
	    if ($id_fn != null && is_numeric($id_fn)) $reqId_fn = " WHERE id_fn = :id_fn ";
	    else $reqId_fn = '';
	    
	    $req = $this->_bdd->prepare("SELECT COUNT(*) AS count FROM words ".$reqId_fn);
	    
	    if ($id_fn != null && is_numeric($id_fn)) $req->bindValue(':id_fn', $id_fn, PDO::PARAM_INT);
	    $cache = $req->executeWithCache(null, 0, 'nb_words_'.$id_fn);
	    
	    return $cache->fetch(PDO::FETCH_ASSOC)['count'];
	}
    }
