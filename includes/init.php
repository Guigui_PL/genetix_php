<?php

	if ( !defined('Framework') ) exit;
	
	/* La variable cheminRacine sert à savoir où l'on se trouve par rapport à la racine du site
	Si elle n'est pas définie, on la définit afin d'éviter des warnings pour avoir utilisé une variable non définie */
	if (!isset($cheminRacine)) $cheminRacine = '';
	
	// On set le timezone par défaut si cela n'a pas été fait dans le php.ini, cela évite des warnings sur les dernières versions de PHP5
	date_default_timezone_set ('Europe/Paris');
	
	// On lance la session, c'est un préalable pour pouvoir utiliser la superglobale $_SESSION
	session_start(); 
	
	// Lorque que l'on fait appel à une classe qui n'existe pas, on essaie d'inclure le fichier la contenant
	/*function __autoload($class)
	{
		if (file_exists($GLOBALS['cheminRacine'].'includes/classes/'.$class.'.php')) require_once ('classes/'.$class.'.php'); 
		else if ($class == 'Smarty') require_once($GLOBALS['cheminRacine'].'libs/smarty/Smarty.class.php');
		else if (strpos($class, 'Smarty') !== FALSE)
		{
			require_once($GLOBALS['cheminRacine'].'libs/smarty/Autoloader.php');
			Smarty_Autoloader::autoload($class);
		}
	}*/
	
	spl_autoload_register(function ($class) 
	{
		if (file_exists($GLOBALS['cheminRacine'].'includes/classes/'.$class.'.php')) require_once ('classes/'.$class.'.php'); 
		else if ($class == 'Smarty') require_once($GLOBALS['cheminRacine'].'libs/smarty/Smarty.class.php');
		else if (strpos($class, 'Smarty') !== FALSE)
		{
			require_once($GLOBALS['cheminRacine'].'libs/smarty/Autoloader.php');
			Smarty_Autoloader::autoload($class);
		}
	});

	// Tentative de connexion à la bdd
	// Si une erreur est retournée, on la capture, on la retourne et on arrête le chargement de la page
	try { $bdd = DB::getInstance(); }
	catch (Exception $exception) { die('Erreur : ' . $exception->getMessage()); }
	
	// Mise en cache de la page
	if (!isset($cache)) $cache = true;
	if (!isset($_SESSION['membre']) && $_SERVER['REQUEST_METHOD'] != 'POST' && $cache == true) // On exclut le cas de l'utilisateur connecté ou de l'envoi d'un formulaire
	{
		$nomCache = 'cache'.urlencode($_SERVER['REQUEST_URI']); // On crée le nom de l'objet à partir de l'URL
		$$nomCache = new Cache($nomCache, 3600); // On crée notre objet cache en indiquant le nom du fichier en cache et sa durée avant rafraîchissement en secondes
		$cacheManager = new CacheManager; // On crée note objet cacheManager pour gérer le cache
		if ($cacheManager->readCache($$nomCache) !== false) // On tente d'obtenir le cache - l'exécution du script s'arrêtera si le fichier existe en cache
		{
			echo $$nomCache->getContenu();
			$cache = false;
			require("piedDePage.php");
			exit;
		}
	}
	
	// On tente de compresser en gzip pour réduire le temps de chargement
	if (!headers_sent() && ob_get_length() == 0)
	{
		if (ini_get('output_handler') == 'ob_gzhandler' || version_compare(PHP_VERSION, '4.2.0') == -1) ob_start();
		 else  ob_start('ob_gzhandler');
	}
	else ob_start();
	
	// Mise en place du système de traduction
	/*if (!isset($_SESSION['langue']))
		$langue = $_SESSION['langue'] = new Langue('FR');
	else $langue = $_SESSION['langue'];*/
	$langue = new Langue('FR');
	
	function t ($phrase, $array = null) { return $GLOBALS['langue']->getTraduction($phrase, $array); }
	
?>