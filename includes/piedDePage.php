<?php

	if ( !defined('Framework') ) exit;
	
	// Écriture du cache
	if (!isset($_SESSION['membre']) && $_SERVER['REQUEST_METHOD'] != 'POST' && $cache)
	{
		$page = ob_get_contents(); // copie du contenu du tampon dans une chaîne
		$$nomCache->setContenu($page); // On met le contenu de notre page dans l'objet de cache
		ob_end_clean(); // effacement du contenu du tampon et arrêt de son fonctionnement
		$cacheManager->writeCache($$nomCache); // On écrit le cache
		echo $page ; // on affiche notre page :D
	}
	
	$tplPied = new Smarty;
	// Création de l'objet smarty
	
	$tempsGeneration = round(microtime(true)-$_SERVER["REQUEST_TIME_FLOAT"], 4);
	// On calcule le temps de génération de la page à 4 décimales près
	// On prend le temps actuel en secondes avec un niveau de précision en millisecondes, on lui retire le moment à partir duquel la page a commencé à charger contenu dans la superglobale $_SERVER["REQUEST_TIME_FLOAT"]
	
	// On assigne afin de pouvoir afficher dans le template
	$tplPied->assign(array(
		'tempsGeneration' => $tempsGeneration,
		'requetes' => $bdd->count(),
		'temps' => round($bdd->time(), 4)
		));
	
	$tplPied->display('piedDePage.html');
	
	if (isset($langue)) $langue->writeConfiguration();

	?>