<?php

	define('Framework', true);
	$cache = false;
	require_once('includes/init.php');
	$titre = t('Chercher des séquences génétiques');
	$tpl = new Smarty;
	
	if (isset($_GET['fonction'])) 
	{
	    try 
	    {
		if (empty($_GET['fonction']))
		    throw new exception(t('La séquence ne peut être vide'));
		
		$fonction = str_replace('-', '+', urldecode($_GET['fonction']));
		
		$logic = new Logic($fonction);
		$veritas = new VeritasLogic($logic);
		
		setcookie ("fonction", $fonction, time() + 365*24*3600);
		
		$tpl->assign(array(
			'logic' => $logic,
			'veritas' => $veritas));
			
		$tpl->display('fonction.html');
	    }
	    catch (Exception $e)
	    {
		$tpl->assign(array(
			'erreur' => $e->getMessage()));
		
		$tpl->display('erreurAjax.html');
	    }
	}
	else if (isset($_GET['fonctionSearch'])) 
	{
	    try 
	    {
		if (empty($_GET['fonctionSearch']))
		    throw new exception(t('La séquence ne peut être vide'));
		
		$fonction = str_replace('-', '+', urldecode($_GET['fonctionSearch']));
		
		$logic = new Logic($fonction);
		$veritas = new VeritasLogic($logic);
		
		setcookie ("fonction", $fonction, time() + 365*24*3600);
		
		$wordsManager = new WordsManager($bdd);
		$logicManager = new LogicManager($bdd);
 		
		$pagination = new Pagination(30, $wordsManager->getNombre($logicManager->getLogic(array('output',$veritas->getMinimalOutput ()))->getId_fn()), 'listSeq.php?output='.$fonction);
		if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
		$pagination->setPremier(false);
		
		$liste =  $wordsManager->getListe($pagination, array('output',$veritas->getMinimalOutput ()), array(['champ' => 'nb_genes', 'sens' => DB::ORDRE_ASC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));
		    
		$tpl->assign(array(
			'listeSequences' => $liste,
			'pages' => $pagination->getPages()));
		
		$tpl->display('listSeq.html');
	    }
	    catch (Exception $e)
	    {
		$tpl->assign(array(
			'erreur' => $e->getMessage()));
		
		$tpl->display('erreurAjax.html');
	    }
	}
	else 
	{
	    if (isset($_COOKIE['fonction']))
		$tpl->assign(array(
			'fonction' => $_COOKIE['fonction']));
	    else
		$tpl->assign(array(
			'fonction' => ''));
		    
	    require_once('includes/entete.php'); 
	    $tpl->display('searchSeq.html');
	    require_once('includes/piedDePage.php');
	} 
