<?php

	define('Framework', true);
	$cache = false;
	require_once('includes/init.php');
	$titre = t('Liste des fonctions');
	$tpl = new Smarty;
	
	require_once('includes/entete.php');
	
	$logicManager = new LogicManager($bdd);
	
	$pagination = new Pagination(30, $logicManager->getNombre(), 'index.php');
	if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
	$pagination->setPremier(true);
		
	$tpl->assign(array(
		'listeFonctions' => $logicManager->getListe($pagination, null, array(['champ' => 'nb_inputs', 'sens' => DB::ORDRE_ASC], ['champ' => 'output', 'sens' => DB::ORDRE_ASC])),
		'pages' => $pagination->getPages()));
	
	$tpl->display('index.html');
	require_once('includes/piedDePage.php');