<?php

	define('Framework', true);
	$titre = 'Séquences';
	$cache = false;
	require_once('includes/init.php');
	$tpl = new Smarty;
	require_once('includes/entete.php');
	
	$wordsManager = new WordsManager($bdd);
	$logicManager = new LogicManager($bdd);
	
	if (isset($_GET['output']))
	{
	    $pagination = new Pagination(30, $wordsManager->getNombre($logicManager->getLogic(array('output', $_GET['output']))->getId_fn()), 'listSeq.php?output='.$_GET['output']);
	    if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
	    $pagination->setPremier(false);
	}
	else
	{
	    $pagination = new Pagination(30, $wordsManager->getNombre(), 'listSeq.php');
	    if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
	    $pagination->setPremier(true);
	}
	    
	if (isset($_GET['output']))
	    $liste =  $wordsManager->getListe($pagination, array('output', $_GET['output']), array(['champ' => 'nb_genes', 'sens' => DB::ORDRE_ASC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));
	else
	    $liste =  $wordsManager->getListe($pagination, null, array(['champ' => 'nb_genes', 'sens' => DB::ORDRE_ASC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));
		
	$tpl->assign(array(
		'listeSequences' => $liste,
		'pages' => $pagination->getPages()));
	
	$tpl->display('listSeq.html');
	require_once('includes/piedDePage.php');