<?php

	define('Framework', true);
	$cache = false;
	require_once('includes/init.php');
	$titre = t('Interpréter une séquence génétique');
	$tpl = new Smarty;
	
	if (isset($_GET['sequence'])) 
	{
	    try 
	    {
		if (empty($_GET['sequence']))
		    throw new exception(t('La séquence ne peut être vide'));
		
		$word = new Word(urldecode($_GET['sequence']));
		$word->exceptionsIfInvalid();
		
		setcookie ("sequence", urldecode($_GET['sequence']), time() + 365*24*3600);
		
		$tpl->assign(array(
			'word' => $word,
			'veritas' => new VeritasWord($word)));
			
		$tpl->display('sequence.html');
	    }
	    catch (Exception $e)
	    {
		$tpl->assign(array(
			'erreur' => $e->getMessage()));
		
		$tpl->display('erreurAjax.html');
	    }
	}
	else 
	{
	    if (isset($_COOKIE['sequence']))
		$tpl->assign(array(
			'sequence' => $_COOKIE['sequence']));
	    else
		$tpl->assign(array(
			'sequence' => ''));
		    
	    require_once('includes/entete.php'); 
	    $tpl->display('interSeq.html');
	    require_once('includes/piedDePage.php');
	}