<?php  if ( !defined('Framework') ) exit;  $traductions = array (
  'Liste des fonctions' => 'Liste des fonctions',
  'Voir les séquences' => 'Voir les séquences',
  'Page' => 'Page',
  'Page générée en %e seconde(s).' => 'Page générée en %e seconde(s).',
  'Avec %e requête(s) SQL exécutée(s) en %e seconde(s).' => 'Avec %e requête(s) SQL exécutée(s) en %e seconde(s).',
  'Voir les détails' => 'Voir les détails',
  'Entrez une séquence' => 'Entrez une séquence',
  'Voir les séquences implémentant la même fonction.' => 'Voir les séquences implémentant la même fonction.',
  'Chercher des séquences génétiques' => 'Chercher des séquences génétiques',
  'Entrer une fonction' => 'Entrer une fonction',
  'Interpréter une séquence génétique' => 'Interpréter une séquence génétique',
  'Entrer une séquence' => 'Entrer une séquence',
  'chargement' => 'chargement',
); 
 ?>