<?php 

	if ( !defined('Framework') ) exit;
	
	$listeFichiers = array(
		"bots.txt" => ["type" => Configuration::TYPE_TABLEAU_TEXTE, "separation" => "|"],
		"langue/FR/langue.php" => ["type" => Configuration::TYPE_TABLEAU_PHP, "variables" => ["traductions"]]
	);
	
?>
